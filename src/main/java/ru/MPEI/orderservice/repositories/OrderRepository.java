package ru.MPEI.orderservice.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.MPEI.orderservice.models.Order;

public interface OrderRepository extends JpaRepository<Order, Long> {
}
