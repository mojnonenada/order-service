package ru.MPEI.orderservice.servicies;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.sleuth.Span;
import org.springframework.cloud.sleuth.Tracer;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;
import ru.MPEI.orderservice.dtos.InventoryResponse;
import ru.MPEI.orderservice.dtos.OrderLineItemsDto;
import ru.MPEI.orderservice.dtos.OrderRequest;
import ru.MPEI.orderservice.event.OrderPlacedEvent;
import ru.MPEI.orderservice.models.Order;
import ru.MPEI.orderservice.models.OrderLineItems;
import ru.MPEI.orderservice.repositories.OrderRepository;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class OrderService {

    private final OrderRepository orderRepository;
    private final WebClient.Builder webClientBuilder;
    private final Tracer tracer;
    private final KafkaTemplate<String, OrderPlacedEvent> kafkaTemplate;
    private final static String INVENTORY_URI = "http://inventory-service/api/inventory";

    public String placeOrder(OrderRequest orderRequest) {
        Order order = new Order();
        order.setOrderNumber(UUID.randomUUID().toString());

        List<OrderLineItems> orderLineItems = orderRequest.getOrderLineItemsDtoList()
                .stream()
                .map(this::mapToDto)
                .toList();

        order.setOrderLineItemsList(orderLineItems);

        List<String> skuCodes = order.getOrderLineItemsList().stream()
                .map(OrderLineItems::getSkuCode)
                .toList();

        log.info("Вызов Inventory-service");

        Span InventoryServiceSpan = tracer.nextSpan().name("InventoryService");
        try(Tracer.SpanInScope inScope = tracer.withSpan(InventoryServiceSpan.start())) {
            // Вызов сервиса инвенторизации
            InventoryResponse[] inventoryResponsesArray = webClientBuilder.build().get()
                    .uri(INVENTORY_URI,
                            uriBuilder -> uriBuilder.queryParam("skuCode", skuCodes).build())
                    .retrieve()
                    .bodyToMono(InventoryResponse[].class)
                    .block();

            boolean allProductsInStock = Arrays.stream(inventoryResponsesArray)
                    .allMatch(InventoryResponse::isInStock);

            if (allProductsInStock) {
                orderRepository.save(order);
                kafkaTemplate.send("notificationTopic", new OrderPlacedEvent(order.getOrderNumber()));
                return "Заказ успешно создан";
            } else {
                throw new IllegalArgumentException("Товара нет на складе");
            }
        } finally {
            InventoryServiceSpan.end();
        }
    }

    private OrderLineItems mapToDto(OrderLineItemsDto orderLineItemsDto) {
        OrderLineItems orderLineItems = new OrderLineItems();
        orderLineItems.setPrice(orderLineItemsDto.getPrice());
        orderLineItems.setQuantity(orderLineItemsDto.getQuantity());
        orderLineItems.setSkuCode(orderLineItemsDto.getSkuCode());
        return orderLineItems;
    }
}